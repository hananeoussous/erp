import { createSlice } from "@reduxjs/toolkit";
import workerData from '../workerData'

const initialState = {
  cart: [],
  items: workerData,
  totalServices: 0,
  totalPrice: 0,
};

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    addToCart: (state, action) => {
      let find = state.cart.findIndex((item) => item.id === action.payload.id);
      if (find >= 0) {
        state.cart[find].Services += 1;
      } else {
        state.cart.push(action.payload);
      }
    },

    getCartTotal: (state) => {
      let { totalServices, totalPrice } = state.cart.reduce(
        (cartTotal, cartItem) => {
          console.log("carttotal", cartTotal);
          console.log("cartitem", cartItem);
          const { price, Services} = cartItem;
          console.log(price, Services);
          const itemTotal = price * Services;
          cartTotal.totalPrice += itemTotal;
          cartTotal.totalServices += Services;
          return cartTotal;
        },
        {
          totalPrice: 0,
          totalServices: 0,
        }
      );
      state.totalPrice = parseInt(totalPrice.toFixed(2));
      state.totalServices = totalServices;
    },

    removeItem: (state, action) => {
      state.cart = state.cart.filter((item) => item.id !== action.payload);
    },
    increaseItemServices: (state, action) => {
      state.cart = state.cart.map((item) => {
        if (item.id === action.payload) {
          return { ...item, Services: item.Services + 1 };
        }
        return item;
      });
    },
    decreaseItemServices: (state, action) => {
      state.cart = state.cart.map((item) => {
        if (item.id === action.payload) {
          return { ...item, Services: item.Services - 1 };
        }
        return item;
      });
    },
  },
});

export const {
  addToCart,
  getCartTotal,
  removeItem,
  increaseItemServices,
  decreaseItemServices,
} = cartSlice.actions;

export default cartSlice.reducer;