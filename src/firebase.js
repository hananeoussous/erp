
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyA-9_aOVkfTJFlSwRSid1MNoEbXzConJbI",
  authDomain: "authentication-tutorial-5de09.firebaseapp.com",
  projectId: "authentication-tutorial-5de09",
  storageBucket: "authentication-tutorial-5de09.appspot.com",
  messagingSenderId: "362030522449",
  appId: "1:362030522449:web:b7cbd26f2b33fdacff5870",
  measurementId: "G-65VQCR2994"
};


const app = initializeApp(firebaseConfig);
const auth = getAuth();

 export {app, auth};