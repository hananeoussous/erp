import React from "react";
import {
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
  MDBCardImage,
  MDBBtn,
  MDBContainer,
  MDBRow,
  MDBCol,
} from "mdb-react-ui-kit";
import { useSelector, useDispatch } from "react-redux";
import { addToCart } from "../features/cartSlice";
import "mdb-react-ui-kit/dist/css/mdb.min.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import { ToastContainer, toast } from "react-toastify";



export default function ProductCard() {
  const items = useSelector((state) => state.allCart.items);
  const notify =()=> toast('this service add in cart in navbar');
  const dispatch = useDispatch();

  return (
    <div className="m-2">
      <MDBContainer>
        <MDBRow className="mb-3">
          {items.map((item) => (
            <MDBCol key={item.id} size="md">
              <MDBCard>
                <MDBCardImage src={item.img} position="top" alt="..." />
                <MDBCardBody>
                  <MDBCardTitle  >{item.title}</MDBCardTitle>
                  <MDBCardTitle  >{item.category}</MDBCardTitle>
                  <MDBCardText>{item.price}</MDBCardText>
                  <button className=' flex items-center text-amber-50 bg-[#cc8947]
            hover:bg-white hover:text-[#CD853F] duration-200 ease-out gap-2
             py-1 px-1 sm:px-4 rounded-full'
                   onClick={() => dispatch(addToCart(item))}>
                    Add Services
                    <ToastContainer />
                  </button>
                </MDBCardBody>
              </MDBCard>
            </MDBCol>
          ))}
        </MDBRow>
      </MDBContainer>
      <div className="m-2">
      <MDBContainer>
        <MDBRow className="mb-3">
          {items.map((item) => (
            <MDBCol key={item.id} size="md">
              <MDBCard>
                <MDBCardImage src={item.img} position="top" alt="..." />
                <MDBCardBody>
                  <MDBCardTitle  >{item.title}</MDBCardTitle>
                  <MDBCardTitle  >{item.category}</MDBCardTitle>
                  <MDBCardText>{item.price}</MDBCardText>
                  <button 
                   onClick={() => dispatch(addToCart(item))}>
                    Add Services
                  </button>
                </MDBCardBody>
              </MDBCard>
            </MDBCol>
          ))}
        </MDBRow>
      </MDBContainer>
    </div>
    </div>
  );
}