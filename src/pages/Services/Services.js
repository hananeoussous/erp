import React, { useState } from 'react'
import { Link } from 'react-router-dom';
import { BsChevronCompactLeft, BsChevronCompactRight } from 'react-icons/bs';
import { RxDotFilled } from 'react-icons/rx';




<script src="https://cdn.tailwindcss.com"></script>

const Services = () => {

  const slides = [
    {
      url: 'images/images (1).jpeg',
    },
    
    {
      url: 'images/téléchargement (1).jpeg',
    },
    {
      url: 'images/images (1).jpeg',
    },
    {
      url: 'images/travailleur-reparant-chauffe-eau.jpg',
    },
    {
      url: 'images/téléchargement (2).jpeg',
    },
    {
      url: 'images/young-engineer-adjusting-autonomous-heating.jpg',
    },
    {
      url: 'images/service-maintenance-worker-repairing (3).jpg',
    },
    {
      url: 'images/service-maintenance-worker-repairing (2).jpg',
    },
    {
      url: 'images/repair-service.jpg',
    },
    {
      url: 'images/pipe-1821109_1280.jpg',
    },
    {
      url: 'images/man-raises-jack-4x4-off-road-truck.jpg',
    },
    {
      url: 'images/man-mechanic-woman-customer-look-car-hood-discuss-repairs.jpg',
    },
    {
      url: 'images/man-electrical-technician-working-switchboard-with-fuses.jpg',
    },
    {
      url: 'images/copper-1189550_1280.jpg',
    },
    {
      url: 'images/image-auto-accident-involving-two-cars.jpg',
    },
    {
      url: 'images/erp.jpg',
    },
    {
      url: 'images/téléchargement.jpeg',
    },
    {
      url: 'images/images.jpeg',
    },
  ];

  const [currentIndex, setCurrentIndex] = useState(0);

  const prevSlide = () => {
    const isFirstSlide = currentIndex === 0;
    const newIndex = isFirstSlide ? slides.length - 1 : currentIndex - 1;
    setCurrentIndex(newIndex);
  };

  const nextSlide = () => {
    const isLastSlide = currentIndex === slides.length - 1;
    const newIndex = isLastSlide ? 0 : currentIndex + 1;
    setCurrentIndex(newIndex);
  };

  const goToSlide = (slideIndex) => {
    setCurrentIndex(slideIndex);
  };

  return (
    <>
       <div className='md:px-14 p-4 max-w-s mx-auto space-y-10'>
      <div className='flex flex-col md:flex-row-reverse justify-between items-center gap-8'>
        <div className='md:w-1/2'>
          <img src='images/about.webp' alt=''></img>
        </div>
        {/*about content */}
        <div className='md:w-1/2'>
        <h1 className='md:text-5xl text-3xl font-extrabold  mb-2 text-[#efe8aa]'>Our
         <span className='text-[#995009] '>services</span></h1>

                    <div >
           <div> <h4 className='text-[25px] text-[#995009]'>•Plumbing: </h4> <p className='text-tartiary 
            mx-auto px-4 text-[19px] text-lg '>
     - Pipe repair and installation.<br></br>
     - Maintenance of water heaters.<br></br>
     - Repairing leaks and drainage.</p></div>
         <div> <h4 className='text-[25px] text-[#995009]'>•Mechanical technician: </h4> 
         <p className='text-tartiary  mx-auto px-4 text-[19px]'>  - Repair and 
         maintenance of mechanical machines.<br></br>

         - Car repair and body repair services.</p></div>
         <div><h4 className='text-[25px] text-[#995009]'>•Electrical technician: </h4>
         <p className='text-tartiary
           mx-auto px-4  text-[19px]'>
         -Repair of home appliances (washing machines, refrigerators, microwaves...)
          electrical appliances.<br></br> 
         -Installation and maintenance of pumps and cooling systems.<br></br>
         - Diagnosing and repairing electrical system malfunctions...</p></div>
         <div><h4 className='text-[25px] text-[#995009]'>•Interior and exterior paints:</h4>
         <p className='text-tartiary
          mx-auto     px-4 text-[19px]'>
         - Walls and ceiling of homes or commercial spaces.<br></br>
         - Building facades and windows
  Surface preparation:<br></br>
  - Preparing surfaces before painting, including sanding surfaces, filling holes, and preliminary cleaning.<br></br>
  Furniture paints:<br></br>
     - Renovating and painting wooden or metal furniture.<br></br>
  Special coatings:<br></br>
     - Apply special coatings such as moisture<br></br>-resistant or mold<br></br>-resistant paint.
     <br></br>-Color consulting
          </p></div>
         </div><br></br>
          
            
          
        </div>
      </div>
    </div>

  
    <div className='max-w-[750px] h-[550px] w-full m-auto py-16 px-9 relative group'>
      <div
        style={{ backgroundImage: `url(${slides[currentIndex].url})` }}
        className='w-full h-full rounded-2xl bg-center bg-cover duration-500'
      ></div>
      {/* Left Arrow */}
      <div className='hidden group-hover:block absolute top-[50%] -translate-x-0 translate-y-[-50%] left-5 text-2xl rounded-full p-2 bg-black/20 text-white cursor-pointer'>
        <BsChevronCompactLeft onClick={prevSlide} size={30} />
      </div>
      {/* Right Arrow */}
      <div className='hidden group-hover:block absolute top-[50%] -translate-x-0 translate-y-[-50%] right-5 text-2xl rounded-full p-2 bg-black/20 text-white cursor-pointer'>
        <BsChevronCompactRight onClick={nextSlide} size={30} />
      </div>
      <div className='flex top-4 justify-center py-2'>
        {slides.map((slide, slideIndex) => (
          <div
            key={slideIndex}
            onClick={() => goToSlide(slideIndex)}
            className='text-2xl cursor-pointer'
          >
            <RxDotFilled />
          </div>
        ))}
      </div>
    </div>

    </>
  )
}

export default Services ;