import React from 'react'
import { Link } from 'react-router-dom';
import './about.css';
;



<script src="https://cdn.tailwindcss.com"></script>


const About = () => {
  const userPic = 'default-pic.jpg';
const userText = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
  const userTexts = document.getElementsByClassName('user-text');
  const userPics = document.getElementsByClassName('user-pic');
  

  function showReview(event) {


    for(userPic of userPics) {
        userPic.classList.remove("active-pic");
    }
    for(userText of userTexts) {
        userText.classList.remove("active-text");
    }
    let i = Array.from(userPics).indexOf(event.target);
    userPics[i].classList.add('active-pic');
    userTexts[i].classList.add('active-text');
    

    
}
 
  return (
    <>
     <div>
                <ul class="hidden lg:flex items-center space-x-6">
                    <li><button class="bg-color-secondary px-9 py-3 rounded-md capitalize font-bold hover:opacity-80 ease-in duration-200">free trial</button></li>
                </ul>
            </div>
    <div className='md:px-14 p-4 max-w-s mx-auto '>
      <div className='flex flex-col md:flex-row justify-between items-center gap-8'>
      <div className='md:w-1/2'>
          <img src='images/Electrician (1).jpg' alt=''></img>
        </div>
      
        {/*about content   variants={FaDeviantart("down",0.2)}
        initial="hidden"
        whileInview={"show"}
        viewport={{once: false,amount: 0.7}}
        className="md:w-1/2"   */}
        <div className='md:w-2/5'>
          <h2 className='md:text-5xl text-3wl font-bold  mb-5 leading-normal '> Exactly <br></br>
         <span className='text-x-2  text-[#995009]'>who  we are: </span ></h2>
          <p className='text-tartiary text-lg mb-7 text-[18px]'>We are a team of experts specialized in diverse
           repair fields, striving to
             provide the highest levels of quality and professionalism.</p>
           
          
        </div>
      </div>
    </div>
    <section id="testimonial">
    <div class="container py-20">
    <div class="text-center m-auto mb-20 md:w-1/2">
                    <h4 class="font-bold text-color-secondary mb-4">User Review</h4>
                    <h1 class="title">What Clients Say About Our App After Use It</h1>
                </div>
    <div class="mt-8">
    <div class="flex items-center justify-center flex-wrap">
                        <img src="./images/téléchargement (4).jpeg" alt="" class="h-20 w-20 rounded-full p-1 m-3 cursor-pointer user-pic active-pic" onclick="showReview()" />
                        <img src="./images/images (2).jpeg" alt="" class="h-20 w-20 rounded-full p-1 m-3 cursor-pointer user-pic" onclick="showReview()" />
                        <img src="./images/images (3).jpeg" alt="" class="h-20 w-20 rounded-full p-1 m-3 cursor-pointer user-pic" onclick="showReview()" />
                        <img src="./images/images (5).jpeg" alt="" class="h-20 w-20 rounded-full p-1 m-3 cursor-pointer user-pic" onclick="showReview()" />
                        <img src="./images/images (4).jpeg" alt="" class="h-20 w-20 rounded-full p-1 m-3 cursor-pointer user-pic" onclick="showReview()" />
                    </div>
    <div class="grid place-items-center text-center m-auto md:w-4/5 min-h-[40vh]">
    <div class="user-text active-text">
                            <p class="md:text-2xl mb-6">"My experience with the repair site was great. I found the service I needed easily and my request was handled quickly and efficiently. The quality of work was excellent and the prices were reasonable.
                             I will definitely come back again if I need repair service in the future"!</p>
                            <h4 class="font-bold text-color-secondary mb-1">amine Nesus</h4>
                            <p>user</p>
                        </div>
    <div class="user-text">
                            <p class="md:text-2xl mb-6">My experience with the repair site was generally positive. I found the service I needed easily and my request was dealt with effectively. The work was thorough and the problem was completely fixed. I am completely
                             satisfied with the service and would recommend it to others.</p>
                            <h4 class="font-bold text-color-secondary mb-1">Maryam Dali</h4>
                            <p>user</p>
                        </div>
    <div class="user-text">
                            <p class="md:text-2xl mb-6">"My experience with the repair site was excellent. The response was prompt and professional, and the repair was carried out very efficiently and with excellent quality. The service was very efficient and reasonably
                             priced. I will definitely return to use their services again in the future"!</p>
                            <h4 class="font-bold text-color-secondary mb-1">ali Karol</h4>
                            <p>user</p>
                        </div>
    <div class="user-text">
                            <p class="md:text-2xl mb-6">"I loved the site design and the ease of navigating between pages.  The purchasing process was smooth and without any problems.  Received the order on time, and the quality of the
                             products was excellent.  I will definitely be back to buy more in the future"!</p>
                            <h4 class="font-bold text-color-secondary mb-1">yaya Jmeawi</h4>
                            <p>user</p>
                        </div>
    <div class="user-text">
            <p class="md:text-2xl mb-6">"My experience with the repair site was good, but there were some downfalls regarding the quality of service. The repair was performed correctly and on time, but I noticed some defects after a 
            short while. The quality of work needs to be improved to ensure customer satisfaction"!</p>
                <h4 class="font-bold text-color-secondary mb-1">Leila frkos</h4>
                  <p>user</p>
                     </div>
                     </div>
                     </div>
                     </div>
                    </section>

  
    </>
    
  )
}

export default About;