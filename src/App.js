import React from 'react';
import './App.css';
import Navbar from './components/Navbar/Navbar';
import Filters from './components/Filters';
import Home from './pages/Home/Home';

import About from './pages/About/About';
import Services from './pages/Services/Services';
import { BrowserRouter, Route, Routes , Router } from 'react-router-dom';
import Newsletteerr from './components/Newsletteerr';
import Login from './components/Login/Login';
import Signup from './components/Signup/Signup';
import ContactUs from './pages/Contact/Contact';
import Worker from './pages/Worker';
import CartPage from './cmp/cartPage';



function App() {
  return (
    <>
    <BrowserRouter>
        <Navbar/>
        <Filters />
        
       <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/about' element={<About />} />
        <Route path='/services'   element={<Services />}/>
        <Route path='/contact' element={<ContactUs />} />
        <Route path='/works' element={<Worker />}/>
        <Route path='/cart' element={<CartPage />} />
        <Route path='/newsletteerr'   element={< Newsletteerr/>}/>
        <Route path='login' element={<Login />} />
        <Route path='signup' element={<Signup />} />
       </Routes>
    </BrowserRouter>

    
    </>
  );
}

export default App;

