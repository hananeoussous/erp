import React from 'react'
<script src="https://cdn.tailwindcss.com"></script>

const Features = () => {
  return (
    <div className='my-24 md:px-14 px-4 max-w-screen-2wl mx-auto '>
         <div className='flex flex-col lg:flex-row justify-between items-start gap-10'>
         <div className='lg:w-1/4'>
            <h2 className='text-4xl   font lg:w-1/2 mb-3 text-[#995009] text-[42px]'>why we are better  then others</h2>
            <p className='text-base  text-tartiary  '> Fast response and repair to provide
             the shortest outage period for your device
  Competitive prices with ongoing offers and discounts
  Customer satisfaction is our first goal and we guarantee it with every service we provide.</p>
         </div>
         {/*fatured cards */}
         <div className='w-full lg:w-3/4'>
          <div className='grid md:grid-cols-3 sm:grid-cols-2 grid-cols-1 items-start md:gap-12 gap-8'>
            <div className='bg-[rgba(248, 231, 142, 0.8)] rounded-[35px] h-96 shadow-3xl
             p-8 items-center flex justify-center items-center hover:-translate-y-4
              transition-all duration-300  cursor-pointer'>
                <div>
                    <img src='images/Animation - 179.jpg' alt=''/><h5 className='text-2xl 
                    font-semibold  px-5 text-center mt-5 text-[#e38b36]' > technical expertise
                     and professional skills</h5>
                </div>
            </div>
            <div className='bg-[rgba(248, 231, 142, 0.8)] rounded-[35px] h-96 shadow-3xl 
             p-8 items-center flex justify-center items-center hover:-translate-y-4
              transition-all duration-300 cursor-pointer md:mt-16  '>
                <div>
                    <img src='images/p2.gif' alt=''/><h5 className='text-2xl 
                    font-semibold  px-5 text-center mt-5 text-[#e38b36]'>Save time, money and peace
                     of mind</h5>
                </div>
            </div>
            <div className='bg-[rgba(248, 231, 142, 0.8)] rounded-[35px] h-96 shadow-3xl
             p-8 items-center flex justify-center items-center hover:-translate-y-4
              transition-all duration-300 '>
                <div>
                    <img src='images/p.gif' alt=''/><h5 className='text-2xl 
                    font-semibold  px-5 text-center mt-5 text-[#e38b36]'> High quality, efficiency and precision</h5>
                </div>
            </div>
            </div>
         </div>
         </div>
    </div>
  )
}

export default Features ;