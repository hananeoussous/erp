import React, { useState ,useEffect} from 'react'
import { TfiMenu } from "react-icons/tfi";
import { FaUser } from "react-icons/fa";
import { HiClipboardDocument, HiXMark } from "react-icons/hi2";
import onClick from 'react';
import { NavItem } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { getCartTotal } from '../../features/cartSlice';
import { useDispatch, useSelector } from 'react-redux';

const Navbar = () => {
  const { cart, totalQuantity } = useSelector((state) => state.allCart);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCartTotal());
  }, [cart]);
  const [isMenuOpen,setIsMenuOpen] = useState(false);
  const toggleMenu = () => {
    setIsMenuOpen(!isMenuOpen);
  }
  const [isLoginOpen,setIsLoginOpen] = useState(false);
  const toggleLogin = () => {
    setIsLoginOpen(!isLoginOpen);
  }

  return (
    
    <>
   

    <div className="flex items-center justify-between bg-[#cc8947] px-2 border-b">

      {/*Left Menu*/ }
      <div className='md:hidden'>
        <button onClick={toggleMenu} className="w-6 h-6 text-amber-50 " data-collapse-toggle="navbar-default" aria-controls="navbar-default" aria-expanded="false" type="button"  >
      {
        isMenuOpen ? (<HiXMark className='w-5 h-5' />) : (<TfiMenu className='w-5 h-5'/> )
      }
      </button>
      </div>
   
        {/*logo     <div className={`space-y-4 px-4 pt-5 pg-5 bg-secondary ${isMenuOpen ? "block fixed top-0 right-0 left-0" : "hidden"}`}>
        {
          NavItem.map(({button}) => <a key={button} href={button} 
          className='block hover:text-gray-300'>{button}</a>)
        }
      </div>*/ }
        <div className="h-14 flex object-cover -my-0  flex items-center p-4  ">
  <a  href="index.html" className='text-2xl font-semiblod flex items-cnter space-x-2 
  text-primary' >
  <svg id="logo-70" width="64" height="700" viewBox="0 0 78 25" fill="none" 
  xmlns="http://www.w3.org/2000/svg"> <path d="M18.5147 0C15.4686 0 12.5473 1.21005 10.3934
   3.36396L3.36396 10.3934C1.21005 12.5473 0 15.4686 0 18.5147C0 24.8579 5.14214 30 11.4853 
   30C14.5314 30 17.4527 28.7899 19.6066 26.636L24.4689 21.7737C24.469 21.7738 24.4689 21.7736
    24.4689 21.7737L38.636 7.6066C39.6647 6.57791 41.0599 6 42.5147 6C44.9503 6 47.0152 7.58741
     47.7311 9.78407L52.2022 5.31296C50.1625 2.11834 46.586 0 42.5147 0C39.4686 0 36.5473 
     1.21005 34.3934 3.36396L15.364 22.3934C14.3353 23.4221 12.9401 24 11.4853 24C8.45584 
     24 6 21.5442 6 18.5147C6 17.0599 6.57791 15.6647 7.6066 14.636L14.636 7.6066C15.6647 
     6.57791 17.0599 6 18.5147 6C20.9504 6 23.0152 7.58748 23.7311 9.78421L28.2023 
     5.31307C26.1626 2.1184 22.5861 0 18.5147 0Z" class="ccustom" fill="#FEFBEB"></path> 
     <path d="M39.364 22.3934C38.3353 23.4221 36.9401 24 35.4853 24C33.05 24 30.9853 22.413
      30.2692 20.2167L25.7982 24.6877C27.838 27.8819 31.4143 30 35.4853 30C38.5314 30 41.4527 
      28.7899 43.6066 26.636L62.636 7.6066C63.6647 6.57791 65.0599 6 66.5147 6C69.5442 6 72
       8.45584 72 11.4853C72 12.9401 71.4221 14.3353 70.3934 15.364L63.364 22.3934C62.3353 
       23.4221 60.9401 24 59.4853 24C57.0498 24 54.985 22.4127 54.269 20.2162L49.798 24.6873C51.8377
        27.8818 55.4141 30 59.4853 30C62.5314 30 65.4527 28.7899 67.6066 26.636L74.636 19.6066C76.7899
         17.4527 78 14.5314 78 11.4853C78 5.14214 72.8579 0 66.5147 0C63.4686 0 60.5473 1.21005
          58.3934 3.36396L39.364 22.3934Z" class="ccustom" fill="#394149"></path> </svg>
  <span className="flex  items-center text-amber-50 text-[25px] " >reparation.com</span></a>
        </div>
        {/* Links */}
        {/*Right  Authentification */}
        <div className='w-8 h-8  bg-[#e20112] rounded-full text-amber-50 flex items-center 
        justify-center lg:hidden'>
       
        <button onClick={toggleLogin} className="w-5 h-6 text-amber-50 " data-collapse-toggle="navbar-default" aria-controls="navbar-default" aria-expanded="false" type="button"  >
      {
        isLoginOpen ? (<HiXMark className='w-5 h-5' />) : (<FaUser className="text-[21px]"/> )
      }
      </button>
        </div>

        <div className='hidden lg:flex items-center '>

        <div className='rounded-full py-2  text-[17px] px-4   text-amber-50 bg-[#cc8947] 
        hover:bg-white   hover:text-[#CD853F]' >
          <Link className='btn ' to="/cart">Cart({totalQuantity})</Link>
        </div>
         
      
        
        <div className='rounded-full py-2  text-[17px] px-4   text-amber-50 bg-[#cc8947] 
        hover:bg-white   hover:text-[#CD853F]'>
        <Link to="/login">
          <button style={{color:'black'}} >Log in</button>
          </Link>
        </div>
        <div className=' p-2 text-[#cc8947] rounded-full px-4 ml-0   text-amber-50 bg-[#e20112]
         hover:bg-white hover:text-[#CD853F]'>
          <Link to="/signup">
             <button >
              <span style={{color:'black'}} >Sign up</span>
              </button>
              </Link>
      </div>
      </div>

      </div>
      </>
  );
};

export default Navbar ;