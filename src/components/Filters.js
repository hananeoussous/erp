import React from 'react'
import { IoHomeSharp } from "react-icons/io5";
import { TiThLarge } from "react-icons/ti";
import { MdBuild } from "react-icons/md";
import { MdHomeRepairService } from "react-icons/md";
import { Link } from 'react-router-dom';

const Filters = () => {
  return  (
  <div className='sm:mx-7 md:mx-10 lg:mx-12  '>
    <div className='flex justify-center gap-8 mt-4  '>
        {/*sorting.map((obj) =>())*/}
            <Link className=' flex items-center text-amber-50 bg-[#cc8947]
            hover:bg-white hover:text-[#CD853F] duration-200 ease-out gap-2
             py-1 px-1 sm:px-4 rounded-full'  to="/" >
              <IoHomeSharp /> 
             <span>Home </span>
              </Link>
             <Link className=' flex items-center text-amber-50 bg-[#cc8947]
             hover:bg-white hover:text-[#CD853F] duration-200 ease-out gap-2
              py-1 px-1 sm:px-4 rounded-full'  to="/about" >
                <TiThLarge  /> 
              <span>About </span>
               </Link>
              <Link className=' flex items-center text-amber-50 bg-[#cc8947]
              hover:bg-white hover:text-[#CD853F] duration-200 ease-out gap-2
               py-1 px-1 sm:px-4 rounded-full'  to="/services" ><MdBuild  /> 
               <span>Services </span> </Link>
               <Link className=' flex items-center text-amber-50 bg-[#cc8947]
               hover:bg-white hover:text-[#CD853F] duration-200 ease-out gap-2
                py-1 px-1 sm:px-4 rounded-full'  to="/contact" ><MdHomeRepairService /> 
                <span>Contact</span> </Link>
                <Link className=' flex items-center text-amber-50 bg-[#cc8947]
               hover:bg-white hover:text-[#CD853F] duration-200 ease-out gap-2
                py-1 px-1 sm:px-4 rounded-full'  to="/works" ><IoHomeSharp /> 
                <span>Workers</span> </Link>
        
        
    </div>
  </div>
)};

export default Filters ;

